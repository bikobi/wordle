package main

import (
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/bikobi/wordle/pkg/forms"
	"gitlab.com/bikobi/wordle/pkg/wordle"
)

func (app *application) home(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "home.page.tmpl", &templateData{})
}

func (app *application) game(w http.ResponseWriter, r *http.Request) {
	// TODO manage error
	session, _ := app.store.Get(r, "game-session")

	if session.IsNew {
		session.Values["game"] = wordle.NewGame(wordle.RandomWord())

		if err := session.Save(r, w); err != nil {
			app.serverError(w, err)
		}
	}

	game, ok := session.Values["game"].(*wordle.Game)
	if !ok {
		app.serverError(w, errors.New(fmt.Sprintf("game is of type %T, need *wordle.Game", session.Values["game"])))
	}

	app.render(w, r, "game.page.tmpl", &templateData{
		Form: forms.New(nil),
		Game: game,
	})
}

func (app *application) submitGuess(w http.ResponseWriter, r *http.Request) {
	session, err := app.store.Get(r, "game-session")
	if err != nil {
		app.serverError(w, err)
	}

	game, ok := session.Values["game"].(*wordle.Game)
	if !ok {
		app.serverError(w, errors.New(fmt.Sprintf("game is of type %T, need *wordle.Game", session.Values["game"])))
	}

	/* Parse form and check for errors. */
	err = r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	form.ExactLength("guessInput", 5)
	form.IsOnlyLetters("guessInput")
	form.IsWord("guessInput")

	if !form.Valid() {
		app.render(w, r, "game.page.tmpl", &templateData{
			Form: form,
			Game: game,
		})
		return
	}

	guess := form.Get("guessInput")
	_, err = game.MakeGuess(guess)
	if err == wordle.ErrWrongLength {
		form.Errors.Add("guessInput", err.Error())
	} else if err != nil {
		app.serverError(w, err)
	}

	if err := session.Save(r, w); err != nil {
		app.serverError(w, err)
	}

	app.render(w, r, "game.page.tmpl", &templateData{
		Form: form,
		Game: game})
}

func (app *application) newGame(w http.ResponseWriter, r *http.Request) {
	session, err := app.store.Get(r, "game-session")
	if err != nil {
		app.serverError(w, err)
	}

	session.Options.MaxAge = -1

	if err := session.Save(r, w); err != nil {
		app.serverError(w, err)
	}

	http.Redirect(w, r, "/game", http.StatusSeeOther)
}
