package main

import (
	"fmt"
	"html/template"
	"path/filepath"
	"strings"

	"gitlab.com/bikobi/wordle/pkg/forms"
	"gitlab.com/bikobi/wordle/pkg/wordle"
)

type templateData struct {
	CurrentYear int
	Form        *forms.Form
	Game        *wordle.Game
}

func splitToChars(s string) []string {
	return strings.Split(s, "")
}

func add(nums ...int) int {
	sum := 0
	for _, num := range nums {
		sum += num
	}
	return sum
}

func subtract(n, m int) int {
	return m - n
}

var functions = template.FuncMap{
	"splitToChars": splitToChars,
	"add":          add,
	"subtract":     subtract,
}

// newTemplateCache creates a cache of template sets from a certain directory.
func newTemplateCache(dir string) (map[string]*template.Template, error) {
	cache := map[string]*template.Template{}

	// 'pages' contains the paths of all the files within 'dir' ending in '.page.tmpl'.
	pages, err := filepath.Glob(filepath.Join(dir, "*.page.tmpl"))
	if err != nil {
		return nil, err
	}
	fmt.Println(pages)

	// for each of such files...
	for _, page := range pages {
		name := filepath.Base(page)

		// template.FuncMap must be registered with the template set *before*
		// calling ParseFiles(). We create a new template set, we register
		// the FuncMap with .Funcs() and then parse the files regularly.
		ts, err := template.New(name).Funcs(functions).ParseFiles(page)
		if err != nil {
			return nil, err
		}

		ts, err = ts.ParseGlob(filepath.Join(dir, "*.layout.tmpl"))
		if err != nil {
			return nil, err
		}

		// ...add any 'partial.tmpl' file...
		ts, err = ts.ParseGlob(filepath.Join(dir, "*.partial.tmpl"))
		if err != nil {
			return nil, err
		}

		// ...and add the template set to the cache.
		cache[name] = ts
	}

	return cache, nil
}
