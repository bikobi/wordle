package main

import (
	"net/http"

	"github.com/bmizerany/pat"
	"github.com/justinas/alice"
)

func (app *application) routes() http.Handler {
	standardMiddleware := alice.New(app.recoverPanic, app.logRequest)

    // dynamicMiddleware := alice.New()

	mux := pat.New()

	// pat matches patterns in the order that they are registered.
	mux.Get("/", http.HandlerFunc(app.home))

	mux.Get("/game", http.HandlerFunc(app.game))
	mux.Post("/game", http.HandlerFunc(app.submitGuess))

    mux.Get("/newgame", http.HandlerFunc(app.newGame))

	fileServer := http.FileServer(http.Dir("./ui/static/"))
	mux.Get("/static/", http.StripPrefix("/static", fileServer))

	return standardMiddleware.Then(mux)
}
