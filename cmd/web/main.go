package main

import (
	"encoding/gob"
	"flag"
	"html/template"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/sessions"
	"gitlab.com/bikobi/wordle/pkg/wordle"
)

type application struct {
	templateCache map[string]*template.Template
	errorLog      *log.Logger
	infoLog       *log.Logger
	store         *sessions.CookieStore
}

func init() {
	gob.Register(&wordle.Game{})
}

func main() {
	addr := flag.String("addr", ":8080", "HTTP network address")
	secret := flag.String("secret", "zsnFhmgKK6gJdgmvLJBPoi2KRVStmNw2", "Secret key")
	flag.Parse()

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)                // Destination, prefix, flags (joined with bitwise OR operator |).
	errLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile) // Lshortfile: filename and line numeber.

	templateCache, err := newTemplateCache("./ui/html/")
	if err != nil {
		log.Fatal(err)
	}

	store := sessions.NewCookieStore([]byte(*secret))

	app := &application{
		templateCache: templateCache,
		errorLog:      errLog,
		infoLog:       infoLog,
		store:         store,
	}

	srv := &http.Server{
		Addr:         *addr,
		Handler:      app.routes(),
		IdleTimeout:  90 * time.Second,
		ReadTimeout:  6 * time.Second,
		WriteTimeout: 12 * time.Second,
	}

	log.Printf("Starting server on %s", *addr)
	err = srv.ListenAndServe()
	log.Fatal(err)
}
