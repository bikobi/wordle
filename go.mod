module gitlab.com/bikobi/wordle

go 1.21.11

require (
	github.com/bmizerany/pat v0.0.0-20210406213842-e4b6760bdd6f
	github.com/gorilla/sessions v1.3.0
	github.com/justinas/alice v1.2.0
)

require github.com/gorilla/securecookie v1.1.2 // indirect
