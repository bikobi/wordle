package forms

import (
	"fmt"
	"net/url"
	"unicode"
	"unicode/utf8"

	"gitlab.com/bikobi/wordle/pkg/wordle"
)

type Form struct {
	url.Values
	Errors errors
}

// New returns an initialized instance of Form.
func New(data url.Values) *Form {
	return &Form{
		data,
		errors(map[string][]string{}),
	}
}

// ExactLength checks that field contains exactly d characters. If it does not, an
// error is added to the Form's Errors.
func (f *Form) ExactLength(field string, d int) {
	value := f.Get(field)
	if value == "" {
		return
	}

	if utf8.RuneCountInString(value) != d {
		f.Errors.Add(field, fmt.Sprintf("This field must be exactly %d chars long", d))
	}
}

// IsOnlyLetters checks that the field contains only a..z and A..Z. If it does not, an
// error is added to the Form's Errors.
func (f *Form) IsOnlyLetters(field string) {
	value := f.Get(field)

	for _, c := range value {
		if !unicode.IsLetter(c) || ((c < 'a' || c > 'z') && (c < 'A' || c > 'Z')) {
			f.Errors.Add(field, "This field can only contain letters a..z and A..Z")
		}
	}
}

// IsWord checks that the field is a real English word. If it is not, an
// error is added to the Form's Errors.
func (f *Form) IsWord(field string) {
	value := f.Get(field)
	if !wordle.IsWord(value) {
		f.Errors.Add(field, "This field must be a real English word")
	}

}

// Valid returns true if Form doesn't contain any error.
func (f *Form) Valid() bool {
	return len(f.Errors) == 0
}
