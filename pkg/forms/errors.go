package forms

type errors map[string][]string

// Add inserts an error message for a given field.
// If the field does not exist, it is created.
func (e errors) Add(field, message string) {
	e[field] = append(e[field], message)
}

// Get retrieves the first error message for a given field. It returns an empty string if the field doesn't contain any message.
func (e errors) Get(field string) string {
	es := e[field]
	if len(es) == 0 {
		return ""
	}
	return es[0]
}

