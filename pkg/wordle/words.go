package wordle

import (
	_ "embed"
	"math/rand"
	"slices"
	"strings"
)

func RandomWord() string {
	wordList := strings.Split(words, "\n")
	r := rand.Intn(len(wordList))
	return wordList[r]
}

func IsWord(s string) bool {
	wordList := strings.Split(words, "\n")
	return !(slices.Index(wordList, strings.ToLower(s)) == -1)
}

// The file `words.txt` must contain exactly one five-letter word per line.
// Source: https://github.com/tabatkins/wordle-list/
// TODO validate the file to avoid unexpected behavior.
//
//go:embed words.txt
var words string
