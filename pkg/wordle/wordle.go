package wordle

import (
	"errors"
	"strings"
)

type Game struct {
	Word      string
	Guesses   []string
	Feedbacks []Feedback
}

// Feedback holds the information about each letter of Guess, compared to a Game.word.
// When a Feedback struct is populated by MakeGuess, the value "true" is found exactly once for
// each index among the three lists.
type Feedback struct {
	Guess string
	// In Correct, "true" is assigned to each letter that is present in the word in the same position as in Guess.
	Correct []bool
	// In Present, "true" is assigned to each letter that is present in the word, but in a different position.
	Present []bool
	// In Incorrect, "true" is assigned to each letter that is not present in the word.
	Incorrect []bool
}

// NewGame returns an initialized instance of Game.
func NewGame(word string) *Game {
	return &Game{
		Word:      word,
		Guesses:   []string{},
		Feedbacks: []Feedback{},
	}
}

var ErrWrongLength = errors.New("guess must be the same length as the word to guess")

// MakeGuess checks a guess against the Game's word and returns the corresponding Feedback struct.
func (g *Game) MakeGuess(guess string) (Feedback, error) {
	guess = strings.ToLower(guess)

	if len(guess) != len(g.Word) {
		return Feedback{}, ErrWrongLength
	}

	feedback := Feedback{
		Guess:     guess,
		Correct:   make([]bool, len(guess)),
		Present:   make([]bool, len(guess)),
		Incorrect: make([]bool, len(guess)),
	}

	letterCount := make(map[rune]int)
	for _, letter := range g.Word {
		letterCount[letter]++
	}

	// Check for correct letters.
	for i, letter := range guess {
		if letter == rune(g.Word[i]) {
			feedback.Correct[i] = true
			letterCount[letter]--
		}
	}

	// Check for present letters.
	for i, letter := range guess {
		if !feedback.Correct[i] {
			if count, found := letterCount[letter]; found && count > 0 {
				feedback.Present[i] = true
				letterCount[letter]--
			} else {
				feedback.Incorrect[i] = true
			}
		}
	}

	g.Guesses = append(g.Guesses, guess)
	g.Feedbacks = append(g.Feedbacks, feedback)

	return feedback, nil
}

func (g *Game) GetGuesses() []string {
	return g.Guesses
}

func (g *Game) GetFeedbacks() []Feedback {
	return g.Feedbacks
}
