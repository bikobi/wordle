# Wordle

A clone of the NY game [Wordle](https://www.nytimes.com/games/wordle/index.html), written in Go. **WIP**.

Live demo: [swordle.fly.dev](https://swordle.fly.dev).

## Todo

See `//TODO` comments in the code.
